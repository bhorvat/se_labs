def fileToList(filename):
    with open(filename, "r") as open_file:
        line = open_file.readline()
        linelist = []
        while line:
            linelist.append(line.strip())
            #print(line)
            line = open_file.readline()
        return linelist

if __name__ == "__main__":
    prime = fileToList("primenumbers.txt")
    happy = fileToList("happynumbers.txt")
    print("Overlapping numbers are: \n")
    for item in prime:
        if item in happy:
    print(item)