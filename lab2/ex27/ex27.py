gameBoard = [[0, 0, 0],
            [0, 0, 0],
            [0, 0, 0]]

def boardCheck(gameBoard):
    newlist = []
    for item in gameBoard:
        for value in item:
            newlist.append(value)
    return all(newlist)
            
def displayBoard(gameBoard):
    i = 0
    for item in gameBoard:
        print(gameBoard[i])
        i += 1
def updateBoard(list, player):
    global gameBoard
    gameBoard[list[0]][list[1]] = player
    displayBoard(gameBoard)
    
def getCoord(player):
    global gameBoard
    choice = input(f"{player} please enter a move: row, column: ")
    strip = choice.split(",")
    intcoord = [int(x) for x in strip]
    gamecoords = [x - 1 for x in intcoord]
    if type(gameBoard[gamecoords[0]][gamecoords[1]]) is str:
        print("Sorry that choice as already been taken choose again: ")
        altcoords = getCoord(player)
        return altcoords
    return gamecoords

if __name__ == "__main__":
    
    while True:
        print("Game board coordinates are: ")
        print("1 2 3\n2 \n3")
        ply1x = "x"
        ply2o = "o"
        player1 = "Player one (x)"
        player2 = "Player two (o)"
        updateBoard(getCoord(player1), ply1x)
        if boardCheck(gameBoard) is True:
            break
        updateBoard(getCoord(player2), ply2o)
        if boardCheck(gameBoard) is True:
            break
print("Game Over No valid moves left!")