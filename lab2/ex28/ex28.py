def verifyMaxFrom3(num1, num2, num3):
    maxi = verifyMaxFrom2(num1, num2)
    return verifyMaxFrom2(maxi, num3)

#function to define max between two numbers
def verifyMaxFrom2(n1, n2):
    if n1 > n2:
        return n1
    else:
        return n2

print(verifyMaxFrom3(1,2,3))
print(verifyMaxFrom3(1,20,3))
print(verifyMaxFrom3(1,2,30))
print(verifyMaxFrom3(1,1,1))