def verifyWinner(matrix):
    endGame = False
    #validate winner by line 0
    if matrix[0][0] == matrix[0][1] and matrix[0][0] == matrix[0][2] and matrix[0][0] != '':
        endGame = True
        print('The winner is player {}'.format(matrix[0][0]))
    #validate winner by line 1
    elif matrix[1][0] == matrix[1][1] and matrix[1][0] == matrix[1][2] and matrix[1][0] != '':
        endGame = True
        print('The winner is player {}'.format(matrix[1][0]))
    #validate winner by line 2
    elif matrix[2][0] == matrix[2][1] and matrix[2][0] == matrix[2][2] and matrix[2][0] != '':
        endGame = True
        print('The winner is player {}'.format(matrix[2][0]))
    #validate winner by column 0
    elif matrix[0][0] == matrix[1][0] and matrix[0][0] == matrix[2][0] and matrix[0][0] != '':
        endGame = True
        print('The winner is player {}'.format(matrix[0][0]))
    #validate winner by column 1
    elif matrix[0][1] == matrix[1][1] and matrix[0][1] == matrix[2][1] and matrix[0][1] != '':
        endGame = True
        print('The winner is player {}'.format(matrix[0][1]))
    #validate winner by column 2
    elif matrix[0][2] == matrix[1][2] and matrix[0][2] == matrix[2][2] and matrix[0][2] != '':
        endGame = True
        print('The winner is player {}'.format(matrix[0][2]))
    #validate winner by diagonal from left to right
    elif matrix[0][0] == matrix[1][1] and matrix[0][0] == matrix[2][2] and matrix[0][0] != '':
        endGame = True
        print('The winner is player {}'.format(matrix[0][0]))
    #validate winner by diagonal from right to left
    elif matrix[0][2] == matrix[1][1] and matrix[0][2] == matrix[2][0] and matrix[2][0] != '':
        endGame = True
        print('The winner is player {}'.format(matrix[0][0]))
    elif isFull(matrix):
        endGame = True
        print('GAME OVER - NO winner!')
    return endGame


# In[2]:


#function to verify if all fields were filled
def isFull(matrix):
    full = True
    for i in range(0,3):
        for j in range(0,3):
            if matrix[i][j] == '':
                full = False
                break
    return full


# In[3]:


def printMatrix(matrix):
    print ('    |   |   ')
    print ('' +str(matrix[0][0])+ '   | ' +str(matrix[0][1])+ ' | ' +str(matrix[0][2]) )
    print ('    |   |   ')
    print ('---------------')
    print ('    |   |   ')
    print ('' +str(matrix[1][0])+ '  | ' +str(matrix[1][1])+ ' | ' +str(matrix[1][2]) )
    print ('    |   |   ')
    print('-----------------')
    print ('    |   |   ')
    print ('' +str(matrix[2][0])+ '  |' +str(matrix[2][1])+ ' | ' +str(matrix[2][2]) )
    print ('    |   |   ')


# In[5]:


#start the matrix with '', when X is player 1 and 0 is player 2, and '' is free field
game = [['', '', ''], ['', '', ''], ['', '', '']]
p1won = 0
p2won = 0


while(not isFull(game)):
    #player 1 is X
    player1 = input('Player 1 (X) what is your move (in the format row,col) from 1 to 3:')
    vector = player1.strip().split(',')
    row = int(vector[0])-1
    column = int(vector[1])-1
    if game[row][column] == '':
        game[row][column] = 'X'
        printMatrix(game)
        if verifyWinner(game):
            p1won +=1
            print('Score is player 1: {} x player 2: {}'.format(p1won,p2won))
            ask = input('Do you want to play a new game? (Yes or No)')
            if ask == 'No':
                break
            else:
                continue
    else:
        print('ERROR, this position row {} and column {} is already filled, please try another position'.format(row,column))
        printMatrix(game)
    if isFull(game):
        break
    else:
        player2 = input('Player 2 (0) what is your move (in the format row,col) from 1 to 3:')
        vector = player2.strip().split(',')
        row = int(vector[0])-1
        column = int(vector[1])-1
        if game[row][column] == '':
            game[row][column] = '0'
            printMatrix(game)
            if verifyWinner(game):
                p1won +=1
                print('Score is player 1: {} x player 2: {}'.format(p1won,p2won))
                ask = input('Do you want to play a new game? (Yes or No)')
                if ask == 'No':
                    break
                else:
                    continue
                    break
        else:
            print('ERROR, this position row {} and column {} is already filled, please try another position'.format(row,column))
            printMatrix(game)
        if isFull(game):
break