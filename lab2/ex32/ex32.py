#import Exercise22 with ready method to read file and convert in a list
import Exercise22
import random

#Create function which returns a random element from the list
def randomWord(array):
    return random.choice(array)

#fileName
file = 'sowpods.txt'

#call readfile function
words = Exercise22.readFile(file)

#function to display status to the user
def printDisplay(disp):
    result = ''
    for x in disp:
        result += x + ' '
    print(result)

#function to validate each letters typed by the user
def validateLetter(letter):
    global display
    global word
    for n in range(0,len(word)):
        if word[n] == letter:
            index = n
            part1 = display[0:index]
            part2 = display[(index+1):]
            temp  = part1 + letter + part2
            display = temp
    printDisplay(display)

#starting the game
print('Welcome to Hangman!\n')

#fill initial fields
def startGame():
    global display
    global word
    global guesses
    global guessesLeft
    display = ''
    word = ''
    #default word 
    word = randomWord(words)
    #create display to show current status to the user
    display = '_' * len(word)
    #record all attempts from the user
    guesses = set()
    guessesLeft = 6
    
#function to draw hangman game
def printToy(num):
    if num == 6:
        print('''													  \n'''
        '''					        _________________			      \n'''
        '''					        |			      \n'''
        '''					        |			      \n''')
    elif num == 5:
                print('''													  \n'''
        '''					        _________________			      \n'''
        '''					        |			      \n'''
        '''					        |			      \n'''
        '''					_________________			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|_______________|			      \n''')
    elif num == 4:
                  print('''													  \n'''
        '''					        _________________			      \n'''
        '''					        |			      \n'''
        '''					        |			      \n'''
        '''					_________________			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|_______________|			      \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n''' 
        '''					        |                         \n''' )
    elif num == 3:
                 print('''													  \n'''
        '''					        _________________			      \n'''
        '''					        |			      \n'''
        '''					        |			      \n'''
        '''					_________________			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|_______________|			      \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''
        '''				    |___________|             \n'''
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n''' 
        '''					        |                         \n''' )
    elif num == 2:
                 print('''													  \n'''
        '''					        _________________			      \n'''
        '''					        |			      \n'''
        '''					        |			      \n'''
        '''					_________________			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|_______________|			      \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''
        '''				    |___________|___________|             \n'''
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n''' 
        '''					        |                         \n''' )
    elif num == 1:
                       print('''													  \n'''
        '''					        _________________			      \n'''
        '''					        |			      \n'''
        '''					        |			      \n'''
        '''					_________________			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|_______________|			      \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''
        '''				    |___________|___________|             \n'''
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n''' 
        '''					        |                         \n''' 
        '''					       | |                         \n'''               
        '''					      |                            \n'''                             
        '''					    |                          \n'''                             
        '''					   |                             \n'''                                           
        '''					   |                             \n'''                                           
        '''					   |                             \n'''                                           
        '''					   |                             \n'''                                                         
        '''					   |                             \n'''                                           
        '''					   |                             \n'''                                           
        '''					___|                             \n'''                                                         
        '''	                                                  \n''')
    elif num == 0:
                        print('''													  \n'''
        '''					        _________________			      \n'''
        '''					        |			      \n'''
        '''					        |			      \n'''
        '''					_________________			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|               |			      \n'''
        '''					|_______________|			      \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''
        '''					        |                         \n'''
        '''				    |___________|___________|             \n'''
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n'''              
        '''					        |                         \n''' 
        '''					        |                         \n''' 
        '''					       | |                         \n'''               
        '''					      |   |                         \n'''                             
        '''					    |       |                         \n'''                             
        '''					   |         |                         \n'''                                           
        '''					   |         |                         \n'''                                           
        '''					   |         |                         \n'''                                           
        '''					   |         |                         \n'''                                                         
        '''					   |         |                         \n'''                                           
        '''					   |         |                         \n'''       )                      
    
    
#starting the game    
startGame()

while(True):
    #draw status
    printToy(guessesLeft)
    #asking the user to guess
    guess = input('Guess your letter:')
    #verify if the letter was already suggest and then show warning to the user
    if guess in guesses:
        print('ERROR! You already type the letter {}, please try again a new letter\n'.format(guess))
    #if the letter exists in the word
    elif guess in word:
        guesses.add(guess)
        validateLetter(guess)
    #if the letter doesn't exists in the word    
    else:
        guesses.add(guess)
        guessesLeft -= 1
        if guessesLeft == 0:
            printToy(guessesLeft)
            print('You Lose, the right word was {}\n'.format(word))
            #ask to the user if he wants to play a new game or stop
            ask = input('Do you want to play a new game? Yes or No')
            if ask == 'Yes':
                startGame()
                continue
            else:
                print('GAME OVER!\n')
                break
        else:
            print('You have {} incorrect guesses left\n'.format(guessesLeft))
    if word == display:
        print('Congratulations!You figured out the right word {}\n'.format(word))
        #ask to the user if he wants to play a new game or stop
        ask = input('Do you want to play a new game? Yes or No')
        if ask == 'Yes':
            startGame()
            continue
        else:
            print('GAME OVER!\n')
break