from django.urls import path
from . import views


app_name="polls"
urlpatterns=[
path('',views.IndexView.as_view(),name='index'),
#localhost:8000/polls/3
path('<int:pk>',views.DetailView.as_view(), name='detail'),
#localhost#8000/slika/3/upvote
path('slika/<int:image_id>/upvote',views.upvote,name="upvote"),
path('slika/<int:image_id>/downvote',views.downvote,name="downvote")


]